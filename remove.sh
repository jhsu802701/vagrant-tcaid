#!/bin/bash
set -e

vagrant halt -f
vagrant destroy -f
vagrant box remove debian/contrib-buster64
